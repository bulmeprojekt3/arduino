#include <Arduino.h>
#include <Servo.h>

#define DEBUG 0
#define NO_PACKET 0
#define PACKET_READING 1

class MotorHandler{

public:

	void initHandler(int servoPin, int pinForward, int pinBackward, int pinPwm){
		forwardPin = pinForward;
		backwardPin = pinBackward;
		pwmPin = pinPwm;
		servo.attach(servoPin);
		pinMode(forwardPin, OUTPUT);
		pinMode(backwardPin, OUTPUT);
		pinMode(pwmPin, OUTPUT);
		servo.writeMicroseconds(1200);
	}

	void motor(int buffer[]){
		int nSpeed = buffer[0];
		int vSpeed = buffer[1];
		int mspeed = map(vSpeed, 0, 100, 0, 255);
		digitalWrite(forwardPin, nSpeed ? 1 : 0);
		digitalWrite(backwardPin, nSpeed ? 0 : 1);
		analogWrite(pwmPin, mspeed);
	}

	void handleServo(int buffer[]){
		uint8_t vServo1 = buffer[0];
		uint8_t vServo2 = buffer[1];
		uint16_t msec = vServo1 << 8 | vServo2;
		servo.writeMicroseconds(msec);

	}

private:

	int forwardPin, backwardPin, pwmPin;
	Servo servo;

};

class LedHandler{

public:

	Adafruit_PWMServoDriver pwmDriver;

	void handle(int buffer[]){
		int led = buffer[0];
		int r = buffer[1];
		int g = buffer[2];
		int b = buffer[3];
		if (led == 4){
			for (int i = 0; i < 4; i++){
				pwm(i * 3, r);
				pwm(i * 3 + 1, b);
				pwm(i * 3 + 2, g);
				/*pwmDriver.setPWM(i * 3, 0, rgbMap(r));
				pwmDriver.setPWM(i * 3 + 1, 0, rgbMap(b));
				pwmDriver.setPWM(i * 3 + 2, 0, rgbMap(g));*/
			}
		}
		else{
			pwm(led * 3, r);
			pwm(led * 3 + 1, b);
			pwm(led * 3 + 2, g);
			/*pwmDriver.setPWM(led * 3, 0, rgbMap(r));
			pwmDriver.setPWM(led * 3 + 1, 0, rgbMap(b));
			pwmDriver.setPWM(led * 3 + 2, 0, rgbMap(g));*/
		}
	}

	void pwm(int i, int r){
		if (r >= 250){
			pwmDriver.setPWM(i, 4096, 0);
		}
		else{
			pwmDriver.setPWM(i, 0, rgbMap(r));
		}
	}

	void setPwmDriver(Adafruit_PWMServoDriver pwm){
		pwmDriver = pwm;
	}

	int rgbMap(int led){
		return map(led, 0, 256, 0, 4096);
	}

};

class PacketHandler{

private:
	static const int knownPackets = 3;
	/*
		Packet 0: rgb	- [led][r][g][b]
		Packet 1: motor - [negative][speed]
		Packet 2: servo - [servo1][servo2]
		*/
	int packetData[knownPackets] = { 4, 2, 2 };
	int state;
	int buffer[64];
	int length;
	int packetID;
	int maxData;
	Adafruit_PWMServoDriver pwm;
	LedHandler ledHandler;
	MotorHandler motorHandler;

	void onEnd(){
		if (packetID == 0){
			ledHandler.handle(buffer);
		}
		else if (packetID == 1){
			motorHandler.motor(buffer);
		}
		else if (packetID == 2){
			motorHandler.handleServo(buffer);
		}
	}

public:

	void setPwm(Adafruit_PWMServoDriver a){
		pwm = a;
		ledHandler.setPwmDriver(pwm);
		motorHandler.initHandler(3, 2, 4, 5);
	}

	void handleData(int data){
		if (state == NO_PACKET){
			if (data > knownPackets){
				Serial.println("Unknown packet id!");
				return;
			}
			length = 0;
			packetID = data;
			maxData = packetData[packetID];
			state = PACKET_READING;
		}
		else{
			buffer[length++] = data;
			if (length >= maxData){
				state = NO_PACKET;
				onEnd();
			}
		}
	}
};