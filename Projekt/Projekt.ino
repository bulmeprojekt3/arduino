#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include "packet.h"

Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x47);
PacketHandler packets;

void setup() {
	Serial.begin(9600);
	Serial.println("Starting pwm driver");
	pwm.begin();
	pwm.setPWMFreq(1000);
	packets.setPwm(pwm);
	for (int i = 0; i < 12; i++){
		pwm.setPWM(i, 0, 4096);
	}
	Serial.println("Setup finished!");
}

void serialEvent(){
	while (Serial.available() > 0){
		int data = Serial.read();
		packets.handleData(data);
	}
}

void loop() {
}